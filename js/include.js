document.addEventListener("DOMContentLoaded", function () {
    const navContainer = document.getElementById("nav-container");
    const xhr = new XMLHttpRequest();
  
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        navContainer.innerHTML = xhr.responseText;
      }
    };
  
    xhr.open("GET", "views/nav.html", true); // Adjust the path to nav.html if necessary
    xhr.send();
  });
  // Function to fetch and insert the content of porto.html
  function loadPortfolioContent() {
    fetch('views/porto.html')
        .then(response => response.text())
        .then(content => {
            document.getElementById('portfolio-content').innerHTML = content;
        })
        .catch(error => {
            console.error('Error fetching portfolio content:', error);
        });
}

// Call the function to load portfolio content when the page loads
window.addEventListener('DOMContentLoaded', loadPortfolioContent);
  